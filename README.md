Table of Contents
=================

* [Table of Contents](#table-of-contents)
* [[WIP] My Cluster Test Container](#wip-my-cluster-test-container)
* [How to run this on your machine](#how-to-run-this-on-your-machine)
* [Notes](#notes)
* [Issues](#issues)
* [Todo](#todo)
* [Pipeline Configuration](#pipeline-configuration)
   * [Gitlab CI](#gitlab-ci)
   * [Github Actions](#github-actions)
* [Tools installed](#tools-installed)
   * [network utils](#network-utils)
   * [shell utils](#shell-utils)
   * [dev utils](#dev-utils)
   * [kubernetes](#kubernetes)
   * [cloud tools](#cloud-tools)
   * [Custom dotfiles for :](#custom-dotfiles-for-)
* [If you want to tweak this with your personal conf](#if-you-want-to-tweak-this-with-your-personal-conf)
* [Links of tools used/installed](#links-of-tools-usedinstalled)
* [Repo Tree](#repo-tree)

Created by [gh-md-toc](https://github.com/ekalinin/github-markdown-toc)
# [WIP] My Cluster Test Container 
Used for testing new clusters based on the ubuntu 20.04
Feel free to fork and modify as you please
# How to run this on your machine
By running this 
```shell
docker run -ti registry.gitlab.com/frankper/mctc
```
# Notes
* User created for the container has passwordless sudo
# Issues 
* N/A
# Todo 
* Add tests
* Add SSH,GPG,AWS,KUBECONFIG functionality
* Correct typos in README.md as I am sure there are many :) 
* Add github actions configuration

# Pipeline Configuration
## Gitlab CI 
Repo contains a gitlab pipeline [file](https://gitlab.com/frankper/mctc/-/blob/main/.gitlab-ci.yml) to build and publish with every push to main
* an image with "commit sha" tag 
* an image with "latest" tag
* an image with "git tag" tag ,if present in git
## Github Actions
WIP
# Tools installed 
## network utils 
* dnsutils arp-scan speedtest-cli sipcalc traceroute nmap
## shell utils
* sudo wget curl vim
## dev utils
* git
## kubernetes
* jq kubectl
## cloud tools 
* aws-cli
## Custom dotfiles for :
*  ohMybash

# If you want to tweak this with your personal conf
* bash config is [here](https://gitlab.com/frankper/mctc/-/tree/main/utils/dotfiles/bash) 
* If you want to add more apt/system packages add them [here](https://gitlab.com/frankper/mctc/-/blob/main/utils/scripts/apt_packages_to_install.txt)
# Links of tools used/installed
* [Oh My Bash](https://github.com/ohmybash/oh-my-bash/)
# Repo Tree
```
├── .dockerignore
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile
├── LICENSE
├── README.md
└── utils/
   ├── dotfiles/
   │  └── bash/
   │     ├── .bash_logout
   │     ├── .bash_profile
   │     └── .bashrc
   └── scripts/
      ├── apt_packages_to_install.txt
      ├── install_omb.sh
      └── update_my_system.sh
```